import net.didion.jwnl.*;
import net.didion.jwnl.data.*;
import net.didion.jwnl.dictionary.*;
import net.didion.jwnl.dictionary.Dictionary;

import java.io.*;
import java.util.HashMap;

public class WordStemming {

	private Dictionary dic;
	private MorphologicalProcessor morph;
	private boolean IsInitialized = false;  
	public HashMap<String, String> AllWords = null;
	
	
	public WordStemming() {
	
	
		AllWords = new HashMap<String, String>();
		
		try
		{
			JWNL.initialize(new FileInputStream("lib/file_properties.xml"));
			
			dic = Dictionary.getInstance();
			morph = dic.getMorphologicalProcessor();
			((AbstractCachingDictionary)dic).setCacheCapacity (10000);
			IsInitialized = true;
		}
		catch ( FileNotFoundException e )
		{
			System.out.println ( "Error initializing Stemmer: JWNLproperties.xml not found" );
		}
		catch ( JWNLException e )
		{
			System.out.println ( "Error initializing Stemmer: " + e.toString() );
			//e.printStackTrace();
		} 
		
	}
	
	public void unload ()
	{ 
		dic.close();
		Dictionary.uninstall();
		JWNL.shutdown();
	}
	
	public String stemWordWithWordNet ( String word )
	{
		if ( !IsInitialized )
			return word;
		if ( word == null ) return null;
		if ( morph == null ) morph = dic.getMorphologicalProcessor();
		
		IndexWord w;
		try
		{
			w = morph.lookupBaseForm( POS.VERB, word );
			if ( w != null )
				return w.getLemma().toString ();
			w = morph.lookupBaseForm( POS.NOUN, word );
			if ( w != null )
				return w.getLemma().toString();
			w = morph.lookupBaseForm( POS.ADJECTIVE, word );
			if ( w != null )
				return w.getLemma().toString();
			w = morph.lookupBaseForm( POS.ADVERB, word );
			if ( w != null )
				return w.getLemma().toString();
		} 
		catch ( JWNLException e )
		{
			e.printStackTrace();
		}
		return null;
	}
	
	public String stem( String word )
	{
		// check if we already know the word
		String stemmedword = AllWords.get(word);
		if (stemmedword != null) //if already exists, return it
			return stemmedword; 
		
		// throw words with digits
		if ( word.matches(".*\\d+.*") == true )
			return null;
		else	// stem the word
			stemmedword = stemWordWithWordNet (word);
		
		if ( stemmedword != null ) //stemmed successfully
		{
			// add it to hashmap and return the stemmed word
			AllWords.put( word, stemmedword );
		}
		
		return stemmedword;
	}

}

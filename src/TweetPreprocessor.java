import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class TweetPreprocessor{
	private List<Pair<String,String>> _processedTweets = new ArrayList<Pair<String, String>>(); 
	private List<String> stopwords = new ArrayList<String>();
	private String stopFile = "data" + File.separator + "Stopwords.txt";
	
	public TweetPreprocessor(List<Pair<String, String>> list){
		try{
			BufferedReader br = new BufferedReader(new FileReader(stopFile));
			String tempWord = br.readLine();
			
			while(tempWord != null){
				stopwords.add(tempWord);
				tempWord = br.readLine();
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		
		/*for (ListIterator<String> iter = stopwords.listIterator(); iter.hasNext(); ) {
		     System.out.print(iter.next());
		}*/
		WordStemming ws = new WordStemming();
		for(int i=0; i < list.size(); i++){
			StringBuilder sb = new StringBuilder();
			Pair<String,String> tempPair = list.get(i);
			String tempTweet = tempPair.getFirst();
			// remove URL
			String tempTweetRemoveUrl = tempTweet.replaceAll("http\\S+\\s?", " ");
			// remove @user
			String tempTweetRemoveUser = tempTweetRemoveUrl.replaceAll("@\\S+\\s?", " ");
			// remove punctuation
			String[] words = tempTweetRemoveUser.replaceAll("[^a-zA-Z ]", "").toLowerCase().split("\\s+");
			// remove stopwords and perform stemming
			for(int j=0; j<words.length; j++){
				if(!stopwords.contains(words[j])){
					String stemWord = ws.stem(words[j]);
					sb.append(stemWord+ " ");
				}
			}
			_processedTweets.add(new Pair<String, String>(sb.toString(), tempPair.getSecond()));
			if(i==525){
				System.out.println(tempTweet);
				printSentence(words);
				System.out.println(sb.toString());
			}
		}
	}
	
	public void printSentence(String[] stringList){
		for(int i=0; i<stringList.length;i++)
			System.out.print(stringList[i]+" ");
		System.out.print("\n");
	}
	
	public List<Pair<String, String>> getTweets(){
		return _processedTweets;
	}
}
import java.awt.BorderLayout;
import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.util.*;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.SwingConstants;

public class GUI implements DocumentListener{
		
	private JTextArea infoArea = new JTextArea();
	private JTextField tweetInput = new JTextField();
	private JTextField displayResult = new JTextField();
	
	private JButton performAnalysis = new JButton("Analysis", createImageIcon("/src/image/hammer.png"));
	
	private String sentiment = new String();
	
	public static void main(String[] args) {
		new GUI();
	}

	public GUI(){
		
		// Load training files
		String trainFile = "data" + File.separator + "train.csv";
		String devFile = "data" + File.separator + "dev.csv";
		TweetPreprocessor trainTweet = new TweetPreprocessor(CsvParser.read(trainFile));
		TweetPreprocessor devTweet = new TweetPreprocessor(CsvParser.read(devFile));	
		final Classify classifier = new Classify();
		List<Pair<String, String>> fullSet = trainTweet.getTweets();
		fullSet.addAll(devTweet.getTweets());
		try {
			classifier.train(fullSet);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		JFrame mainFrame = new JFrame();
		JPanel mainPanel = new JPanel();

		//make sure the program exits when the frame closes
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.setTitle("TweetPlus V1.0");
		mainFrame.setSize(615,285);
		mainFrame.setResizable(false);

		//This will center the JFrame in the middle of the screen
		mainFrame.setLocationRelativeTo(null);

		mainFrame.getContentPane().setLayout(new BorderLayout());
        mainFrame.getContentPane().add(mainPanel, BorderLayout.CENTER);
        
        mainPanel.setLayout(null);
        mainPanel.setSize(600,285);
        infoArea.setFont(new Font("Narkisim", Font.BOLD, 19));
        infoArea.setText("Enter the Tweet:");
        infoArea.setEditable(false);
        infoArea.setBounds(49, 65, 200, 23);
              
        tweetInput = new JTextField();
        tweetInput.setFont(new Font("Miriam", Font.BOLD, 20));
        tweetInput.setBounds(49, 93, 429, 33);
        
        performAnalysis.setBounds(487,85,48,48);
        displayResult.setHorizontalAlignment(SwingConstants.CENTER);
        displayResult.setFont(new Font("Nirmala UI", Font.BOLD, 42));
        displayResult.setEditable(false);
        
        displayResult.setBounds(185, 147, 213, 57);
        
        mainPanel.add(infoArea);
        mainPanel.add(tweetInput);
        mainPanel.add(performAnalysis);
        mainPanel.add(displayResult);
        
        // ActionListener for text input
        tweetInput.getDocument().addDocumentListener(this);
        
        // ActionListener for performSearch
        performAnalysis.addActionListener(new ActionListener() {
        	 
            public void actionPerformed(ActionEvent e){
                // only perform search if use entered some sweets
            	if(tweetInput.getText().trim().length() != 0){
            		String textEntered = "";
            		if(tweetInput.getText().trim().length() != 0){
            		    textEntered = tweetInput.getText();
            		    System.out.println(textEntered);
            		    List<Pair<String,String>> tweetList = new ArrayList<Pair<String,String>>();
            		    tweetList.add(new Pair<String, String>((textEntered), ""));
            		    TweetPreprocessor singleTweet = new TweetPreprocessor(tweetList);
            		    sentiment = classifier.classify(singleTweet.getTweets().get(0).getFirst());
            		    // Simple negation for not, no and n't
            		    String tempTweet = textEntered.toLowerCase();
            		    if(tempTweet.contains("not")||tempTweet.contains("no")||tempTweet.contains("n't")){
            		    	System.out.println(sentiment);
            		    	if(sentiment.equals("negative")) 
            		    		sentiment = new String("positive");
            		    	else if(sentiment.equals("positive")) 
            		    		sentiment = new String("negative");
            		    	System.out.println(sentiment);
            		    }
            		    System.out.println(sentiment);
            		    String output = sentiment.toUpperCase();
            		    switch(output){
            		    case "POSITIVE": displayResult.setForeground(Color.GREEN);break;
            		    case "NEGATIVE": displayResult.setForeground(Color.RED);break;
            		    default: displayResult.setForeground(Color.BLACK);break;
            		    }
            		    displayResult.setText(output);
            		}
            	}
            }
        });      
        
        mainFrame.setVisible(true);
	}
	
	/** Returns an ImageIcon, or null if the path was invalid. */
    protected static ImageIcon createImageIcon(String path) {
        String iconPath = System.getProperty("user.dir")+ path;
        System.out.println(iconPath);
        File icon = new File(iconPath);
        
        if (icon.exists()) {
            return new ImageIcon(iconPath);
        } else {
            System.err.println("Couldn't find file: " + path);
            return null;
        }
    }

	@Override
	public void changedUpdate(DocumentEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void insertUpdate(DocumentEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeUpdate(DocumentEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
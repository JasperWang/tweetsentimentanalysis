import com.aliasi.util.Files;
import com.aliasi.classify.Classification;
import com.aliasi.classify.Classified;
import com.aliasi.classify.DynamicLMClassifier;
import com.aliasi.lm.NGramProcessLM;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class Classify {
    File mPolarityDir;
    String[] mCategories = {"positive", "negative", "neutral", "irrelevant", "unsure"};
   /**
    * 
    */
    int[][] confusionMatrix;
    int numTests = 0;
    int numCorrect = 0;
    
    
    DynamicLMClassifier<NGramProcessLM> mClassifier;
    public Classify() {
    	int nGram = 8;
        mClassifier 
            = DynamicLMClassifier
            .createNGramProcess(mCategories,nGram);
    }
    
    public void train(List<Pair<String, String>> data) throws IOException {
        int numTrainingCases = 0;
        System.out.println("\nTraining.");
        for(Pair p : data) {

        	String tweets = (String) p.getFirst();
        	String tag = ((String) p.getSecond()).replace(" ", "");
        	Classification classification = new Classification(tag);
            Classified<CharSequence> classified
            = new Classified<CharSequence>(tweets,classification);
            mClassifier.handle(classified);
            numTrainingCases++;
        }
        System.out.println("  # Training Cases=" + numTrainingCases);
   }

    public String classify(String content) {
    	return mClassifier.classify(content).bestCategory();
    }
    public void evaluate(List<Pair<String, String>> test) throws IOException {
        System.out.println("\nEvaluating......");
        confusionMatrix = new int[5][5];
        for(Pair p : test) {
        	String tweets = (String) p.getFirst();
        	String category = (String) p.getSecond();
            Classification classification = mClassifier.classify(tweets);
            resultRecorder(classification.bestCategory(), category);
        }
        resultAnalyser();
    }
    private void resultRecorder(String best, String answer) {
    	int row = 0;
    	int col = 0;
    	for(int i = 0; i < mCategories.length; i++) {
    		if(best.equals(mCategories[i])) col = i;
    		if(answer.equals(mCategories[i])) row = i;
    	}
    	confusionMatrix[row][col]++;
        ++numTests;
        if (best.equals(answer)) {
        ++numCorrect;
        }
    }
    
    private void resultAnalyser() {
        System.out.println("  # Test Cases=" + numTests);
        System.out.println("  # Correct=" + numCorrect);
        System.out.println("  % Correct=" 
                           + ((double)numCorrect)/(double)numTests);
        confusionMatrixPrinter();
    }
    
    public void confusionMatrixPrinter() {
        for(int i = 0; i < mCategories.length; i++) {
        	System.out.println("====");  
        	System.out.print("  % Recall rate for \"" + mCategories[i] + "\"=");
        	int k = 0;
        	for(int j = 0; j < mCategories.length; j++) 
        		k += confusionMatrix[j][i];
        	double recall = 0.0;
        	if(k == 0) recall = 1.00;
        	else recall = (double)confusionMatrix[i][i] / k;
        	System.out.println(recall);
        	System.out.print("  % Precision rate for \"" + mCategories[i] + "\"=");
        	int m = 0;
        	for(int j = 0; j < mCategories.length; j++) 
        		m += confusionMatrix[i][j];
        	double precision = 0.0;
        	if(m == 0) precision = 1.0;
        	else precision = (double)confusionMatrix[i][i] / m;
        	System.out.println(precision);  
        }
    	System.out.println();
        System.out.println("======= Confusion Matrix =======");
    	for(int i = 0; i < confusionMatrix[0].length; i++) 
    		System.out.print((char)('a' + i) + "   ");
    	System.out.println();
        for(int i = 0; i < confusionMatrix[0].length; i++) {
        	for(int j = 0; j < confusionMatrix[i].length; j++) {
        		if(confusionMatrix[i][j] / 100 > 0) {
        			System.out.print(confusionMatrix[i][j] + " ");
        		}
        		else if(confusionMatrix[i][j] / 10 > 0) {
        			System.out.print(confusionMatrix[i][j] + "  ");
        		}
        		else {
        			System.out.print(confusionMatrix[i][j] + "   ");
            		
        		}
        	}
    		System.out.println();
        }
        for(int i = 0; i < confusionMatrix[0].length; i++) 
        	System.out.println((char)('a' + i) + " = " + mCategories[i]);
    }
}


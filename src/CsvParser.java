import java.io.*;
import java.util.ArrayList;
import java.util.List;

import com.opencsv.CSVReader;


public class CsvParser {

	@SuppressWarnings("resource")
	public static List<Pair<String, String>> read(String csvFile) {

		List<Pair<String, String>> result = new ArrayList<Pair<String,String>>();

		CSVReader reader;
		try {
			reader = new CSVReader(new FileReader(csvFile), ',' , '"' , 1);
			
			int i=1;
			String[] tweet;
			while ((tweet = reader.readNext()) != null) {
				if (tweet != null) {
				
					String[] sentimentArray = tweet[4].split(" ");
					String sentiment = sentimentArray[sentimentArray.length-1];
					
					Pair<String, String> pair = new Pair<String, String>(tweet[3], sentiment);
					
					result.add(pair);

					//System.out.println(i+": "+pair.getFirst()+","+pair.getSecond());
					i++;
				}
				
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("Finish Parsing");

		return result;
	}




	public static void main(String[] args) throws IOException {
		String filename = "data" + File.separator + "train.csv";
		String testName = "data" + File.separator + "test.csv";
		String toneName = "data" + File.separator + "dev.csv";
		TweetPreprocessor tp = new TweetPreprocessor(CsvParser.read(filename));
		TweetPreprocessor test = new TweetPreprocessor(CsvParser.read(testName));
		TweetPreprocessor tone = new TweetPreprocessor(CsvParser.read(toneName));
		List<Pair<String, String>> trainFile = tp.getTweets();
		trainFile.addAll(tone.getTweets());
		Classify classifier = new Classify();
		//classifier.train(tp.getTweets());
		classifier.train(trainFile);
		//System.out.println(test.getTweets().get(0).getFirst());
		classifier.evaluate(test.getTweets());

	}
}


